use reqwest;
use clap::Parser;
use std::io;
use std::io::prelude::*;

#[derive(Parser, Clone)]
#[clap(author="Joel Cherney", version, about="\nA drop in replacement for sendmail to send messages to discord.\nOptions \"-t\" and \"-v\" are ignored for now.\nOptions \"-F\" and \"-f\" will both result in the discord username. \"-F\" overrides \"-f\" if both are used.")]
struct Args {
    /// Set the sender's full name.
    #[arg(short = 'F', long, default_value = "Sendmail Wrapper")]
    name: String,

    /// Set the sender's address.
    #[arg(short = 'f', long, default_value = "Sendmail Wrapper")]
    from: String,

    /// (IGNORED) Read the message's To:, Cc:, and Bcc: fields for recipients.  The Bcc: field will be deleted before sending.
    #[arg(short = 't', long, default_value = "Sendmail Wrapper")]
    to: String,

    /// The Discord webhook URL. In sendmail this would be the email address
    #[arg(index = 1)]
    url: String,

    /// (IGNORED) Enable verbose output.
    #[arg(short = 'v', long, default_value = "Sendmail Wrapper")]
    verbose: String,
}


async fn send_message(url: String, username: String, message: String) -> Result<(), reqwest::Error> {
    let json_data: String = format!("{{\"content\": \"{}\", \"username\": \"{}\" }}", message, username);
    let client = reqwest::Client::new();

    let response = client
        .post(url)
        .header("Content-Type", "application/json")
        .body(json_data.to_owned())
        .send()
        .await?;

    match response.status().as_u16() {
        200..=299 => {
            let body = response.text().await?;
            println!("Success! Body:\n{}", body);
        }
        400..=599 => {
            let status = response.status();
            let error_message = response.text().await?;
            println!("Error {}: {}", status, error_message);
        }
        _ => {
            println!("Unexpected status code: {}", response.status());
        }
    }

    //let response_body = response.text().await?;
    //println!("Response body:\n{}", response_body);

    Ok(())
}

fn get_message() -> String {
    // Read our message from stdin
    let mut message = "".to_owned();
    let stdin = io::stdin().lock();
    for line in stdin.lines() {
        message.push_str("\\n");
        message.push_str(line.unwrap().as_str());
    }
    return message

}

#[tokio::main]
async fn main() {
    // Get our CLI arguments
    let args = Args::parse();
    let default_username: String = "Sendmail Wrapper".to_owned();

    // Figure out our username from the arguments
    let username = if args.name != default_username {
        args.name.clone()
    } else {
        args.from.clone()
    };

    // Send the message to discord
    let _ = send_message(args.url, username, get_message()).await;
}
