# Sendmail

This script is a drop in replacement for sendmail that sends to discord instead of a mail server

## Usage

If you run `sendmail --help` you'll receive a comprehensive report on how to use the program.

Options `-t` and `-v` are ignored and only exist to be argument comptaptible with sendmail.
Options `-F` and `-f` will become the Discord username. [Discord and Clyde case insentive cannot be used](https://discord.com/developers/docs/resources/webhook#create-webhook)
    When `-F` is used it overrides `-f` if both are used
Instead of providing the `to` in the sendmail command provide the discord webhook url.

Normal sendmail command of:
`sendmail [-tv] [-F name] [-f from] to ...`
becomes
`sendmail [-Ff] discord_username discord_webhook_url ...`
